#ifndef WRITEONLY_VECTOR_H_
#define WRITEONLY_VECTOR_H_

#include <memory.h>
#include <stdbool.h>
#include <stdlib.h>

static const size_t WOV_T_INIT_CAP = 4;

typedef struct {
  size_t elem_size, capacity, len;
  void* data;
} wov_t;

static inline wov_t* new_wov_t(size_t elem_size) {
  wov_t* v =  (wov_t*)malloc(sizeof(wov_t));
  v->data = malloc(elem_size * WOV_T_INIT_CAP);
  v->elem_size = elem_size;
  v->capacity = WOV_T_INIT_CAP;
  v->len = 0;
  return v;
}

static inline void destroy_wov_t(wov_t* v) {
  free(v);
}

static inline void* wov_t_at(wov_t* v, size_t pos) {
  return (char*)(v->data) + pos * v->elem_size;
}

static inline int wov_t_find(wov_t* v, const void* mem) {
  for (size_t pos = 0; pos < v->len; ++pos) {
    if (memcmp(wov_t_at(v, pos), mem, v->elem_size) == 0)
      return pos;
  }
  return -1;
}

static inline void wov_t_add(wov_t* v, const void* mem) {
  if (v->len + 1 > v->capacity) {
    v->capacity *= 2;
    v->data = realloc(v->data, v->capacity * v->elem_size);
  }
  memcpy((char*)v->data + v->len * v->elem_size, mem, v->elem_size);
  v->len++;
}

#endif
